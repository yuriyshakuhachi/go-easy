import React, {Component} from 'react';
import Field9x9 from "./Field9x9";
import Button from "../layouts/Button";
import Modal from "../layouts/Modal";
import StoneButton from "./StoneButton";
import descriptionImg from "./img/love__.png";
import rulesImg from "./img/wey.png";
import './go.css';


class Go extends Component {
    constructor(props) {
        super(props);
        this.newGame = this.newGame.bind(this)
        this.field9x9 = [
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
        ]
        this.field3x3 = [
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},

            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
            {el: ' ', vdf: 4},
        ]
        this.state = {
            showMod: false,
            question: '',
            textHeader: '',
            actions: '',
        };
        this.openFirstModal = this.openFirstModal.bind(this);
        this.openSecondModal = this.openSecondModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.answer1 = this.answer1.bind(this);
        this.answer2 = this.answer2.bind(this);
        this.answer3 = this.answer3.bind(this);
        this.answer4 = this.answer4.bind(this);
    }

    openFirstModal() {
        this.setState({
            showMod: true,
            textHeader: 'Go Easy v1.0.2',
            question: '   Эта версия представляет собой реализацию древней восточной игры ГО. ' +
                'Еще ее называют китайскими шахматами, хотя вариантов развития событий гораздо больше,' +
                ' чем у традиционных шахмат, так как поле для игры может быть любых размеров. ' +
                'ГO обязательна для вступительных экзаменов многих экономических вузов на востоке. ' +
                'В этой версии игры поле 9x9 и возможностью борьбы двух стихий - огня и воды. В последующем  ' +
                'планируется возможность игры с искусственным интеллектом, который будет учиться у соперника.' +
                'Также будет версия  для  борьбы более чем двух стихий. С благодарностью коллективу "DAN-IT", за ' +
                'организацию учебного процесса. Отдельный респект Донченко Сергею, за вдохновенное изложение ООП. ' +
                'Приятного время провождения и интеллектуального наслаждения! ' +

                'Все права на коммерческое использование ' +
                ' принадлежат Шульге Юрию Викторовичу < yuriyshulga@yahoo.com >.  ',
            actions: <>
                <Button backgroundColor={'#0e5b16'} text={"I agree"} onBClick={this.answer1}/>
                <Button backgroundColor={'#b3382c'} text={"I don't agree"} onBClick={this.answer2}/>
            </>
        });
    }

    openSecondModal() {
        this.setState({
            showMod: true,
            textHeader: '',
            question: 'Традиционно игра ведется между двумя стихиями. Каждый игрок, ' +
                'представляющий свою стихию, стремится погасить или высушить стихию ' +
                'противника. Каждая стихия ходит в свою очередь. Фишки ставятся на ' +
                'пересечение линий. Каждая фишка поставленная на доску имеет степень ' +
                'свободы, которая определяется свободными направлениями по четырем сторонам ' +
                'от нее. Когда степень свободы фишки (элемента) становиться равной нулю, она ' +
                'снимается с доски. Тоже происходит с группой фишек (страной). Побеждает та' +
                ' стихия, элементов которой остается больше. Каждый игрок может пропустить свой ' +
                'ход, если считает его не нужным (следует нажать на фишку над доской, она ' +
                'же показывает чей ход будет следующим). При пропуске хода каждым из игроков (подряд), ' +
                'игра завершается, и определяется победитель.'  ,
            actions: <>
                {/*<Button backgroundColor={'#0e5b16'} text={'No'} onBClick={this.answer3}/>*/}
                {/*<Button backgroundColor={'#b3382c'} text={'Yes'} onBClick={this.answer4}/>*/}
            </>
        })
    }


    answer1() {
        alert('Good luck!!!')
        this.closeModal()
    }

    answer2() {
        alert('Kill yourself!!!')
        this.closeModal()
        window.close()
    }

    answer3() {
        alert('Bye!')
        this.closeModal()
    }

    answer4() {
        alert("let's go back")
        this.closeModal();
        this.openFirstModal();
    }

    closeModal() {
        this.setState({
            showMod: false,
        })
    }

    newGame() {
        window.location.reload();
    }

    render() {
        return <>
            <div className="container">
                < Modal showMod={this.state.showMod}
                       closeButton={true}
                       header={this.state.textHeader}
                       text={this.state.question}
                       closeModal={this.closeModal}
                       actions={this.state.actions}
                />

                <div className="header-footer">
                    <div className="menu-item">
                        <StoneButton onClick={this.openFirstModal} img={descriptionImg}/>
                        Соглашение
                    </div>
                    <div className="menu-item">
                        <StoneButton onClick={this.openSecondModal} img={rulesImg}/>
                        Правила
                    </div>
                </div>

                <Field9x9 field={this.field9x9} newGame={this.newGame}/>
{/*/todo  //*field 3x3*/}

                {/*<Field9x9 field={this.field3x3} newGame={this.newGame}/>*/}
                <div className="header-footer">
                </div>

            </div>
        </>
    }
}

export default Go
