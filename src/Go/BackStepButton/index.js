import Point from "../../layouts/Field/Point";
class BackStepButton extends Point{
    click(){
        this.props.onClick()
    }
    render() {
        return <>
            <div className="field" onClick={this.click}><img src={this.props.img} height="68" width="68" alt=""/>
            </div>
        </>
    }

}

export default BackStepButton