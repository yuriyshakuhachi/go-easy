import Point from "../../layouts/Field/Point";

class CompStepButton extends Point {

    click() {

        let timerId = setInterval(() => this.props.onClick(), 500);
        // остановить вывод через    секунд
        setTimeout(() => { clearInterval(timerId); this.props.onClick(); }, 50000);
    }

    render() {
        return <>
            <div className="field" onClick={this.click}><img src={this.props.img} height="68" width="68" alt=""/>
            </div>
        </>
    }
}

export default CompStepButton