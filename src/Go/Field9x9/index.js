import Field from "../../layouts/Field";

class Field9x9 extends Field {
    render() {
        let nextPoint = this.elNextStep()
        let point = this.renderPoints()
        let backStep = this.elBackStep()
        let compStep = this.elCompStep()
        return <>
            <div className="field9x9big">
                <div className="next-point">{nextPoint}</div>
                <div className="field9x9">
                    <div>{point[0]}</div>
                    <div>{point[1]}</div>
                    <div>{point[2]}</div>
                    <div>{point[3]}</div>
                    <div>{point[4]}</div>
                    <div>{point[5]}</div>
                    <div>{point[6]}</div>
                    <div>{point[7]}</div>
                    <div>{point[8]}</div>
                    <div>{point[9]}</div>
                    <div>{point[10]}</div>
                    <div>{point[11]}</div>
                    <div>{point[12]}</div>
                    <div>{point[13]}</div>
                    <div>{point[14]}</div>
                    <div>{point[15]}</div>
                    <div>{point[16]}</div>
                    <div>{point[17]}</div>
                    <div>{point[18]}</div>
                    <div>{point[19]}</div>
                    <div>{point[20]}</div>
                    <div>{point[21]}</div>
                    <div>{point[22]}</div>
                    <div>{point[23]}</div>
                    <div>{point[24]}</div>
                    <div>{point[25]}</div>
                    <div>{point[26]}</div>
                    <div>{point[27]}</div>
                    <div>{point[28]}</div>
                    <div>{point[29]}</div>
                    <div>{point[30]}</div>
                    <div>{point[31]}</div>
                    <div>{point[32]}</div>
                    <div>{point[33]}</div>
                    <div>{point[34]}</div>
                    <div>{point[35]}</div>
                    <div>{point[36]}</div>
                    <div>{point[37]}</div>
                    <div>{point[38]}</div>
                    <div>{point[39]}</div>
                    <div>{point[40]}</div>
                    <div>{point[41]}</div>
                    <div>{point[42]}</div>
                    <div>{point[43]}</div>
                    <div>{point[44]}</div>
                    <div>{point[45]}</div>
                    <div>{point[46]}</div>
                    <div>{point[47]}</div>
                    <div>{point[48]}</div>
                    <div>{point[49]}</div>
                    <div>{point[50]}</div>
                    <div>{point[51]}</div>
                    <div>{point[52]}</div>
                    <div>{point[53]}</div>
                    <div>{point[54]}</div>
                    <div>{point[55]}</div>
                    <div>{point[56]}</div>
                    <div>{point[57]}</div>
                    <div>{point[58]}</div>
                    <div>{point[59]}</div>
                    <div>{point[60]}</div>
                    <div>{point[61]}</div>
                    <div>{point[62]}</div>
                    <div>{point[63]}</div>
                    <div>{point[64]}</div>
                    <div>{point[65]}</div>
                    <div>{point[66]}</div>
                    <div>{point[67]}</div>
                    <div>{point[68]}</div>
                    <div>{point[69]}</div>
                    <div>{point[70]}</div>
                    <div>{point[71]}</div>
                    <div>{point[72]}</div>
                    <div>{point[73]}</div>
                    <div>{point[74]}</div>
                    <div>{point[75]}</div>
                    <div>{point[76]}</div>
                    <div>{point[77]}</div>
                    <div>{point[78]}</div>
                    <div>{point[79]}</div>
                    <div>{point[80]}</div>
                </div>
                <div className="next-point">{backStep}{compStep}</div>
                {/*<div className="next-point">{compStep}</div>*/}
            </div>
        </>
    }
}

export default Field9x9
