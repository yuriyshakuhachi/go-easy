import Point from "../../layouts/Field/Point";
class StoneButton extends Point {
    click() {
        this.props.onClick()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
    }

    render() {
        return <>
            <div className="field" onClick={this.click}><img src={this.props.img} height="68" width="68" alt=""/>
            </div>
        </>
    }
}
export default StoneButton