import {Element} from "./element";

export class Elements {
    constructor(field, allSteps, el) {
        this._allSteps = allSteps;
        this._field = field;
        this.sizeField = this._field.length;
        this.sideField = Math.sqrt(this._field.length);
        this._el = el;
        this.countries = [];
    }


    /**
     * ставит новый элемент
     * @param id
     * @returns [{*},{*},{*}...]
     */

    go(id) {
        this._element = this.newElement(id, this._el, this._field, this.countries, this._allSteps);
        this._field[id] = {
            element: this._element,
            el: this._el,
            vdf: this._element.valueDegreeOfFreedom(id),
            right: this._element.whoIsRight(id),
            left: this._element.whoIsLeft(id),
            top: this._element.whoIsTop(id),
            bottom: this._element.whoIsBottom(id),
            update: this._element.update(0),
            nextStep: true,
        };
        if (this.countries.length !== 0) {
            for (let i = 0; i < this.countries.length; i++) {
                this._field[this.countries[i]].nextStep = true
            }
        }
        if (this._element.whoIsRight(id) !== '|') {
            this._field[id + 1].left = this._el
        }
        if (this._element.whoIsLeft(id) !== '|') {
            this._field[id - 1].right = this._el
        }
        if (this._element.whoIsTop(id) !== '|') {
            this._field[id - this.sideField].bottom = this._el
        }
        if (this._element.whoIsBottom(id) !== '|') {
            this._field[id + this.sideField].top = this._el
        }
        if (this._field[id].vdf === 0) {
            this._element.doNotDel = true
        }
        if (this._field[id].vdf !== 0) {
            this._element.doNotDel = false
        }
        if (this._el === ' ') {
            this._element.doNotDel = false
        }
        return this._field

    }

    getRandomInt(min = 0, max = 1000) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
    }


    newElement(id, el) {
        return new Element(id, el, this._field, this.countries, this._allSteps);
    }

}