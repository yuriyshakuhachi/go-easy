import React, {Component} from 'react';
import './Point.css';
import imgFire from './img/f.png'
import imgWater from './img/w.png'
import imgEmpty from './img/o.png'

class Point extends Component {

    constructor(props) {
        super(props);
        this.click = this.click.bind(this);
        this.vdf0 = this.vdf0.bind(this);
    }

    update() {
        this.forceUpdate()
    }

    click() {
        this.props.onClick(this.props.id);
    }

    vdf0(id) {
        this.props.vdf0(id)
        this.props.field[id].nextStep = false
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (
            (prevProps.el !== this.props.el) ||
            (prevProps.t !== this.props.t) ||
            (prevProps.r !== this.props.r) ||
            (prevProps.b !== this.props.b) ||
            (prevProps.l !== this.props.l) ||
            (prevProps.vdf !== this.props.vdf) ||
            (prevProps.update !== this.props.update)
        ) {
            if (this.props.field[this.props.id].element) {
                this.props.field[this.props.id].update =
                    this.props.field[this.props.id].element.update(1)
                this.props.field[this.props.id].vdf =
                    this.props.field[this.props.id].element.valueDegreeOfFreedom(this.props.id);
                if (this.props.field[this.props.id].vdf === 0) {
                    this.vdf0(this.props.id)
                } else {
                    this.props.field[this.props.id].nextStep = false
                    if (this.props.field[this.props.id].element) {
                        let idElementsCountry = this.props.field[this.props.id].element.countries;

                        if (idElementsCountry.length !== 0) {
                            for (let i = 0; i < idElementsCountry.length; i++) {
                                this.props.field[idElementsCountry[i]].nextStep = false
                            }
                        }
                    }
                }
            }
        }
    }

    render() {

        const field = (this.props.el === ' ') ? imgEmpty :
            (this.props.el === 'f' ? imgFire :
                imgWater)
        return <>
            <div className="field" onClick={this.click}><img src={field} height="48" width="48" alt=""/></div>
        </>
    }
}

export default Point;
