/** todo исправить ошибку = нет логики следующего хода после шага назад **/
/** todo исправить ошибку подсчета победителя!!*/
/** todo две самообучающиеся сети которые играют друг с другом**/
/** todo убрать логику из компонентов реакта  **/


import React, {Component} from 'react';
import './Field.css';
import Point from "./Point";
import {Elements} from "../elements";
import BackStepButton from "../../Go/BackStepButton";
import CompStepButton from "../../Go/CompStepButton";
import backImg from "../../Go/img/back.png";
import compImg from "../../Go/img/wey.png";

class Field extends Component {
    constructor(props) {
        super(props);
        this.click = this.click.bind(this);
        this.vdf0 = this.vdf0.bind(this);
        this.del = this.del.bind(this);
        this.clickSkipStep = this.clickSkipStep.bind(this);
        this.backStep = this.backStep.bind(this);
        this.compStep = this.compStep.bind(this);
        this.humanStep = 1;
        this.stepCount = 0;
        this.clickSkipStepCount = 0;
        this.allSteps = [];
        this.renderPoint = [];
        this.nextPoint = undefined;
        this.lastEl = 'w';
        this.lastStep = props.field;
        this.state = {
            field: props.field,
        };
        this.countWater = 0;
        this.countFire = 0;
        this.countEmpty = this.state.field.length;

        this.perceptron = localStorage.getItem('perceptron');
        if (this.perceptron === null) {
            // console.log('perceptron->', this.perceptron)
            localStorage.setItem('perceptron', '9x9');
        }
        // console.log('perceptron->', this.perceptron)

    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        // this.compStep()
    }

    click(id, st) {
        console.log('st--->', st);

        /** проверка на финиш*/
        if (this.finish(this.state.field)) {
            return
        }


        /**запрет на ход в занятое поле*/
        if (this.busyPoint(id)) {
            return 'b'
        }


        /** если поле с vdf-0 стало пустым на прошлом ходу,то
         *  ходить в него можно только на следующем ходу
         * */
        if ((this.state.field[id].vdf === 0) && (this.state.field[id].el === ' ')) {
            if (this.allSteps[this.allSteps.length - 1][id].el !== ' ') {
                return
            }
        }

        /**
         * пишем историю ходов
         */
        this.allSteps.push([...this.state.field]);
        ++this.stepCount
        localStorage.setItem('perceptron', localStorage.getItem('perceptron') + '1')
        // console.log('perceptron->', localStorage.getItem('perceptron'))
        /**
         * ставим новый элемент
         */
        if (this.lastEl === 'w') {
            this.fire = new Elements(this.state.field, this.allSteps, 'f');
            this.setState({
                field: this.fire.go(id),
            })
            this.lastEl = 'f'

        } else {
            this.water = new Elements(this.state.field, this.allSteps, 'w');
            this.setState({
                field: this.water.go(id),
            })
            this.lastEl = 'w'
        }
        /**
         // * вызывается генерация хода машины ( если нужен, раскрыть комменты ниже этой строчки )
         */
        // if (st !== 1) {
        //     this.compStep()
        // }
    }

    /**
     * ход машины
     */
    compStep() {
        // let compPasseRandomStep = this.getRandomInt(0, 10) // рандомирует пропуск хода машиной
        let compPasseRandomStep = 1;
        if (compPasseRandomStep !== 0) {
            console.log('numberSteps -> ', compPasseRandomStep)
            let busy
            this.humanStep = 1;
            do {
                busy = this.click((this.getRandomInt(0, 81)), this.humanStep)
            } while (busy === 'b')
        } else {
            console.log('compPasseRandomStep -> 0')
            this.clickSkipStep()
        }
    }

    clickSkipStep(el) {
        this.lastEl = el
        if ((this.clickSkipStepCount === this.stepCount) && (this.stepCount !== 0)) {
            this.finaleCalc()
        }
        this.clickSkipStepCount = this.stepCount
    }

    vdf0(id) {
        if (this.state.field[id].nextStep) {
            return
        }
        /**
         * удаляем элемент или группу элементов если vdf=0
         */
        let idElementsCountry = this.state.field[id].element.countries;
        if (idElementsCountry.length !== 0) {
            for (let i = 0; i < idElementsCountry.length; i++) {
                this.del(idElementsCountry[i])
            }
        } else {
            this.del(id)
        }
    }

    del(id) {
        if (this.state.field[id].el !== ' ') {
            this.emptiness = new Elements(this.state.field, this.allSteps, ' ');
            this.setState({
                field: this.emptiness.go(id),
            })
        }
    }

    busyPoint(id) {
        return this.state.field[id].el !== ' ';
    }

    /**считаем количество элементов */
    count(field) {
        this.countFire = 0
        this.countWater = 0
        this.countEmpty = 0

        field.forEach((item) => {
            switch (item.el) {
                case 'f':
                    this.countFire++
                    break
                case 'w':
                    this.countWater++
                    break
                case ' ':
                    this.countEmpty++
                    break
                default:
            }
        })
    }

    finish(field) {
        this.count(field)
        if (this.countEmpty <= 1) {
            this.setState({
                field: field,
            })
            this.lastEl = ' '

            this.finaleCalc()
            return true
        }
    }

    getRandomInt(min = 0, max = 1000) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
    }

    backStep() {
        console.log(this.allSteps)
        this.setState({
            field: this.allSteps[this.allSteps.length - 1]
        })
        this.lastStep = this.allSteps.pop()
        this.stepCount = this.stepCount - 1;
        if (this.lastEl === 'f') {
            this.lastEl = 'w'
        } else {
            if (this.lastEl === 'w') {
                this.lastEl = 'f'
            }
        }
        console.log(this.lastEl);
        console.log(this.stepCount);
    }

    elBackStep() {
        return <BackStepButton onClick={this.backStep} img={backImg}/>
    }

    elCompStep() {
        return <CompStepButton onClick={this.compStep} img={compImg}/>
    }

    elNextStep() {
        if (this.lastEl === 'f') {
            this.nextPoint = <NextPoint onClick={this.clickSkipStep} el={'w'}/>
        } else {
            if (this.lastEl === 'w') {
                this.nextPoint = <NextPoint onClick={this.clickSkipStep} el={'f'}/>
            }
        }
        return this.nextPoint
    }


    /** подсчет и определение победителя*/
    finaleCalc() {
        this.count(this.state.field)
        if (this.countFire > this.countWater) {
            alert(`Победил огонь ${this.countFire}:${this.countWater}`)
        } else {
            if (this.countFire < this.countWater) {
                alert(`Победила вода ${this.countWater}:${this.countFire}`)
            } else {
                alert(`Победила пустота ${this.countWater}:${this.countFire}`)
            }
        }
        console.log('fire->', this.countFire, ' water->', this.countWater)
        this.props.newGame()
    }

    renderPoints() {
        for (let i = 0; i < this.state.field.length; i++) {
            this.renderPoint[i] =
                <Point id={i} field={this.state.field} el={this.state.field[i].el} t={this.state.field[i].top}
                       r={this.state.field[i].right} b={this.state.field[i].bottom} l={this.state.field[i].left}
                       vdf0={this.vdf0} onClick={this.click} update={this.state.field[i].update}
                       vdf={this.state.field[i].vdf}/>
        }
        return this.renderPoint
    }

}

class NextPoint extends Point {
    click() {
        this.props.onClick(this.props.el)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
    }
}

export default Field