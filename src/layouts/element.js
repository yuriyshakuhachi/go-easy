export class Element {
    constructor(id, el, field, countries, allSteps) {
        this.history = allSteps;
        this._field = field;
        this.sizeField = this._field.length;
        this.sideField = Math.sqrt(this._field.length);
        this.id = id;
        this._el = el;
        /** _elAnti **/
        switch (el) {
            case 'f':
                this._elAnti = 'w'
                break
            case 'w':
                this._elAnti = 'f'
                break
            case ' ':
                this._elAnti = ' '
                break
            default:
        }
        this.freedom = [];
        this.countries = countries;

    }

    update(stepCount) {
        this.countUpdate = this.countUpdate + stepCount
        return this.countUpdate
    }

    /**
     * возвращает количество свободных мест
     * и добавляет в массивы всех соседских братьев
     */

    brothersAndFreedom(id) {
        let value = 0;
        switch (this.whoIsRight(id)) {
            case ' ':
                /**
                 * добавляем степень свободы и пишем в массив свободы
                 */
                value = value + 1;
                if (!this.freedom.includes(id + 1)) {
                    this.freedom.push(id + 1);
                }
                break
            case this._elAnti:
                /**
                 * исключаем из массива свободы, если он там был
                 */
                if (this.freedom.includes(id + 1)) {
                    this.freedom.splice(this.freedom.indexOf(id + 1, 0), 1)
                }
                break
            case this._el:
                /**
                 * добавляем элемент и родственные соседские элементы в массив
                 */
                if (!this.countries.includes(id + 1)) {
                    this.countries.push(id + 1)
                }
                if (!this.countries.includes(id)) {
                    this.countries.push(id);
                }
                /**
                 * исключаем из массива свободы если он там был.
                 */
                if (this.freedom.includes(id + 1)) {
                    this.freedom.splice(this.freedom.indexOf(id + 1, 0), 1)
                }
                break
            case
            '|'
            :
                break
            default:
        }
        switch (this.whoIsBottom(id)) {
            case ' ':
                value = value + 1;
                if (!this.freedom.includes(id + this.sideField)) {
                    this.freedom.push(id + this.sideField)
                }
                break
            case this._elAnti:
                if (this.freedom.includes(id + this.sideField)) {
                    this.freedom.splice(this.freedom.indexOf(id + this.sideField, 0), 1)
                }
                break
            case this._el:
                if (!this.countries.includes(id + this.sideField)) {
                    this.countries.push(id + this.sideField)
                }
                if (!this.countries.includes(id)) {
                    this.countries.push(id);
                }

                if (this.freedom.includes(id + this.sideField)) {
                    this.freedom.splice(this.freedom.indexOf(id + this.sideField, 0), 1)
                }

                break
            case '|':
                break
            default:
        }
        switch (this.whoIsLeft(id)) {
            case ' ':
                value = value + 1;
                if (!this.freedom.includes(id - 1)) {
                    this.freedom.push(id - 1);
                }
                break
            case this._elAnti:
                if (this.freedom.includes(id - 1)) {
                    this.freedom.splice(this.freedom.indexOf(id - 1, 0), 1)
                }
                break
            case this._el:
                if (!this.countries.includes(id - 1)) {
                    this.countries.push(id - 1)
                }
                if (!this.countries.includes(id)) {
                    this.countries.push(id);
                }
                if (this.freedom.includes(id - 1)) {
                    this.freedom.splice(this.freedom.indexOf(id - 1, 0), 1)
                }
                break
            case '|':
                break
            default:
        }
        switch (this.whoIsTop(id)) {
            case ' ':
                value = value + 1;
                if (!this.freedom.includes(id - this.sideField)) {
                    this.freedom.push(id - this.sideField);
                }
                break
            case this._elAnti:
                if (this.freedom.includes(id - this.sideField)) {
                    this.freedom.splice(this.freedom.indexOf(id - this.sideField, 0), 1)
                }
                break
            case this._el:
                if (!this.countries.includes(id - this.sideField)) {
                    this.countries.push(id - this.sideField);
                }
                if (!this.countries.includes(id)) {
                    this.countries.push(id);
                }


                if (this.freedom.includes(id - this.sideField)) {
                    this.freedom.splice(this.freedom.indexOf(id - this.sideField, 0), 1)
                }
                break
            case '|':
                break
            default:
        }
        return value
    }

    /**
     * возвращает степень свободы для группы связанных элементов
     */
    valueCountry(id) {
        for (let i = 0; i < this.countries.length; i++) {
            this.brothersAndFreedom(this.countries[i])
        }
        for (let i = 0; i < this.countries.length; i++) {
            this._field[this.countries[i]].vdf = this.freedom.length
        }
        return this.freedom.length
    }

    /**
     * возвращает степень свободы для элемента
     * @param id
     * @returns {number}
     */
    valueDegreeOfFreedom(id) {
        let value = this.brothersAndFreedom(id);
        if (this.countries.length !== 0) {
            value = this.valueCountry(id)
        }
        return value
    }


    /**
     * @param id
     * @returns el
     */
    whoIsRight(id) {
        const right = id + 1;
        if ((right <= this.sizeField - 1) && (((right / this.sideField) - (Math.trunc(right / this.sideField))) !== 0)) {
            return this._field[right].el
        } else {
            return '|'
        }
    }

    whoIsLeft(id) {
        const left = id - 1;
        if ((left > 0) && (((id / (this.sideField)) - (Math.trunc(id / (this.sideField)))) !== 0)) {
            return this._field[left].el
        } else {
            if (left === 0) {
                return this._field[left].el
            } else {
                return '|'
            }
        }
    }

    whoIsTop(id) {
        const top = id - this.sideField;
        if (top >= 0) {
            return this._field[top].el
        } else {
            return '|'
        }
    }

    whoIsBottom(id) {
        const bottom = id + this.sideField;
        if (bottom <= this.sizeField - 1) {
            return this._field[bottom].el
        } else {
            return '|'
        }
    }


}