import React, {Component} from 'react';
import './Modal.css';
import Button from "../Button";

class Modal extends Component {
    render() {
        const styleClassWindow = this.props.showMod ? 'modal active' : 'modal';
        const styleClassCloseBtn = this.props.closeButton ? 'close active' : 'close';
        return (
            <div className={styleClassWindow}>
                <div className="modal-content">
                    <div className={"header"}>
                        <div className={"header-text"}>{this.props.header}</div>
                        <div className={styleClassCloseBtn}>
                            <Button backgroundColor={'#AA6337'} text={'X'} onBClick={this.props.closeModal}/>
                        </div>
                    </div>
                    <div className="content-text">{this.props.text}</div>
                    <div className={"actions"}>
                        {this.props.actions}
                    </div>
                </div>
            </div>)
    }
}

export default Modal